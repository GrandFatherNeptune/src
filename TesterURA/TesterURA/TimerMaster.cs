﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski


{
    class TimerMaster : Agent
    {
        const double _A = 1.0;
        // tablice 
        private double[] e;
        private double[] u;
        private double[] y;
        private double[] j;
        // parametry
        private double[] a, b;
        // nastawy regulatora
        //private double Tp, Ti, Td, kp;
        private double[] K;
        // iterator zeby wiedziec gdzie jestesmy
        private int n;

        public TimerMaster(int id, double Tp, double Td, double Ti, double kp, float timeStep = 0.1F) : base(id, timeStep)
        {
            // takie tam
            this.timeStep = timeStep;
            this.Id = id;
            this.n = 0;
            
            // tablice zainicjalizowac
            this.e = new double[1000];
            this.u = new double[1000];
            this.y = new double[1000];
            this.j = new double[1000];
            
            // nastawy uklady
            this.a = this.b = new double[2];
            double tp2 = this.timeStep * this.timeStep;
            // policzyc nastawy
            // a0 = a2 !!!
            this.a[0] = tp2 / ( 4.0 + 4*this.timeStep + 3*tp2 );
            this.a[1] = 2 * this.a[0];
            // b1 => b[0] i b2 => b[1] !!!
            this.b[0] = (6 * tp2 - 8.0) / (4.0 + 4 * this.timeStep + 3 * tp2);
            this.b[1] = (4.0 - 4 * this.timeStep + 3 * tp2) / (4.0 + 4 * this.timeStep + 3 * tp2);

            // dodac parametry poczatkowe jako zerowe
            for( int i = 0; i < 3; i++ )
            {
                e[i] = _A;
                u[i] = 0;
                y[i] = 0;
                j[i] = 0;
                Console.WriteLine(e[i] + "\t" + u[i] + "\t" + y[i] + "\t" + j[i]);
            }
            this.n = 3;

            // z nastaw regulatora
            this.K = new double[3];
            this.K[0] = kp * (1.0 + this.timeStep / Ti + Td / this.timeStep);
            this.K[1] = -kp * (1.0 + 2 * Td / this.timeStep);
            this.K[2] = kp * Td / this.timeStep;
            Console.WriteLine("NASTAWY");
            Console.WriteLine(K[0] + "\t" + K[1] + "\t" + K[2]);

        }

        public override void Update()
        {
            // policz e
            calcE();

            // policz u
            calcU();

            // policz y
            calcY();

            // policz j
            calcJ();

            Console.WriteLine(e[n] + "\t" + u[n] + "\t" + y[n] + "\t" + j[n]);
            // zwieksz iterator
            this.n++;
            // <3
            if ( n > 100 )
            {
                this.Finish();
            }
        }

        private void calcE()
        {
            // looool
            e[n] = _A - y[n-1];
        }
        private void calcU()
        {
            // tez to porozbijac
            u[n] = u[n - 1] + K[0] * e[n] + K[1] * e[n - 1] + K[2] * e[n - 2];
        }
        private void calcY()
        {
            // to porozbijac na czlony trzeba bo nie czytelne jest
            y[n] = a[0] * u[n] + a[1] * u[n - 1] + a[0] * u[n - 2] - b[0] * y[n - 1] - b[1] * y[n - 2];
        }
        private void calcJ()
        {
            j[n] = j[n - 1] + Math.Abs(e[n]) * (timeStep * n) * (timeStep * n);
        }

    }
}
