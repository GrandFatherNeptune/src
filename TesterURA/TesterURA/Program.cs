﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class Program
    {


        static void Main(string[] args)
        {
            IRunnable reg = new TimerMaster(0, 0.1, 0.1, 1.0, 0.0f);
            reg.Run();
            

            // czekaj na BUTTON
            // Thread.Sleep(100);
            Console.WriteLine("End of program");
            Console.ReadKey();
        }

        static List<IRunnable> GenerateRunnables()
        {
            var runnables = new List<IRunnable>();
            // tutaj generujemy knypkow

            return runnables;
        }

        static List<Thread> RunThreads(IEnumerable<IRunnable> runnables)
        {
            var threads = new List<Thread>(runnables.Count());

            foreach (var runnable in runnables)
            {
                var t = new Thread(runnable.Run);
                threads.Add(t);
                t.Start();
            }

            return threads;
        }
    }
}
