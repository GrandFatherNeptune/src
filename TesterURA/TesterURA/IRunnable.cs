﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    // zadanie 2.(2.2)
    interface IRunnable
    {
        // metoda Run() ktora odpowiada za odpalenie watku
        void Run();

        // enumerator typu float ktory posiada sekwencje czasow po ktorych nalezy uruchomic agenta
        IEnumerator<float> CoroutineUpdate();

        // informacja czy skonczyl prace - get pozwala ja wyciagnac <Agent>.HasFinished.get() ?
        bool HasFinished { get; }
    }
}
