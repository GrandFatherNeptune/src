﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class ReadFileAgent : Agent
    {
        private string filePath;
        private string textFromFile;
        public List<string> words;

        public ReadFileAgent(int id, string filePath) : base(id)
        {
            this.filePath = filePath;
        }

        public override void Update()
        {
            if ( !File.Exists(filePath)  )
            {
                Console.WriteLine("ERROR: Invalid file path or file doesn't exist");
                System.Environment.Exit(-1);
            }
            textFromFile = File.ReadAllText(filePath);
            //string[] wordsArray = textFromFile.Split(' ');
            //words = textFromFile.Replace('\t',' ').Replace('\n',' ').Replace(',',' ').Split(' ').ToList();
            words = textFromFile.Replace(System.Environment.NewLine," ").Split(' ').ToList();
            Finish();
        }

        public string getText() { return textFromFile; }
    }
}
