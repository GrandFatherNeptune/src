﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class HakerAgentAdd : Agent
    {
        public float balance;
        public BankAgent bank;

        public HakerAgentAdd(int id, BankAgent bank, float timeStep = .5F ) : base(id, timeStep)
        {
            this.bank = bank;
        }

        public override void Update()
        {
            //*// MUTEX CODE
            // zajmij mutexior
            bank.mutex.WaitOne();
            try
            {
                balance = bank.balance + 200;
                bank.balance = balance;
                Console.WriteLine(">> ADD: " + balance);
            }
            catch (Exception excp)
            {
                Console.WriteLine( excp.StackTrace );
            }
            finally
            {
                bank.mutex.ReleaseMutex();
            }
            //*/

            /*/ LOCK CODE
            lock(bank)
            {
                balance = bank.balance + 200;
                bank.balance = balance;
                Console.WriteLine(">> ADD: " + balance);
            }
            //*/

            /*/ SPIN LOCK CODE
            bool spinLockTaken = false;
            try
            {
                bank.spinLock.Enter(ref spinLockTaken);
                balance = bank.balance + 200;
                bank.balance = balance;
                Console.WriteLine(">> ADD: " + balance);
            }
            catch (Exception excp)
            {
                Console.WriteLine(excp.StackTrace);
            }
            finally
            {
                if( spinLockTaken )
                {
                    bank.spinLock.Exit();
                }
            }
            //*/
            /*/ SPIN LOCK CODE -> busy-waiting
            bool spinLockTaken = false;
            while ( !spinLockTaken )
            {
                bank.spinLock.Enter(ref spinLockTaken);
            }
            if ( spinLockTaken )
            {
                // do some stuff
                try
                {
                    balance = bank.balance + 200;
                    bank.balance = balance;
                    Console.WriteLine(">> ADD: " + balance);
                }
                finally {
                    // zwolnij kolege
                    bank.spinLock.Exit();
                }
            }
            //*/

            if (bank.HasFinished) Finish();
        }
    }
}
