﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class WordCountMaster : Agent
    {
        public Dictionary<String, int> slownik;
        private List<WordCountAgent> slavy;

        public WordCountMaster(int id, List<WordCountAgent> slavy) : base(id)
        {
            this.slavy = slavy;
            slownik = new Dictionary<string, int>();
        }

        public override void Update()
        {
            // jesli skonczyly
            if ( slavy.All(a => a.HasFinished) )
            {
                // trzeba je dodac
                foreach (var s in slavy) {
                    // dobra dobra
                    foreach (var sl in s.slownik )
                    {
                        // jesli istnieje to sumujemy
                        if( slownik.ContainsKey( sl.Key ) )
                        {
                            slownik[sl.Key] += sl.Value;
                        }
                        // jesli nie to dodajemy
                        else
                        slownik.Add(sl.Key, sl.Value);
                    }
                }
            }
            // print result
            printResult();
            Finish();
        }

        private void printResult()
        {
            foreach( var s in slownik )
            {
                Console.WriteLine(s.Key + ": " + s.Value);
            }
        }
    }
}
