﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrzybyszSzymanski
{
    class BankAgent : Agent
    {
        public Mutex mutex;
        public SpinLock spinLock;
        public float balance;
        private int N;

        
        public BankAgent(int id, float balance) : base(id)
        {
            this.balance = balance;
            base.timeStep = 2.0f;
            this.mutex = new Mutex();
            this.spinLock = new SpinLock();

            this.N = 0;
        }

        public override void Update()
        {
            //*/ MUTEX CODE
            this.mutex.WaitOne();
            try
            {
                Console.WriteLine("-----------------------");
                Console.WriteLine(">> BANK ACCOUNT: " + balance);
            }
            catch (Exception excp)
            {
                Console.WriteLine(excp.StackTrace);
            }
            finally
            {
                this.mutex.ReleaseMutex();
            }
            //*/

            /*/ LOCK CODE
            Console.WriteLine("-----------------------");
            Console.WriteLine(">> BANK ACCOUNT: " + balance);
            //*/

            /*/ spin lock
            bool spinLockTaken = false;
            while (!spinLockTaken)
            {
                this.spinLock.Enter(ref spinLockTaken);
            }
            if (spinLockTaken)
            {
                // do some stuff
                try
                {
                    Console.WriteLine("-----------------------");
                    Console.WriteLine(">> BANK ACCOUNT: " + balance);
                    Thread.Sleep(1000);
                }
                finally
                {
                    // zwolnij kolege
                    this.spinLock.Exit();
                }
            }
            //*/

            // taka blokada dla blokady
            if (this.N++ > 5)
            {
                Finish();
            }
        }
    }
}
