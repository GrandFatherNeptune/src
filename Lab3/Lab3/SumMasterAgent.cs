﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class SumMasterAgent : Agent
    {
        public int id;
        private List<IRunnable> slavy;
        public int sum;

        public SumMasterAgent(int id, List<IRunnable> slavy) : base(id)
        {
            sum = 0;
            this.id = id;
            this.slavy = slavy;
        }

        public override void Update()
        {
            if (slavy.All(a => a.HasFinished ) ) {
                foreach (var s in slavy.Cast<SumAgent>()) sum += s.sum;
            }
            Console.WriteLine("Suma: " + this.sum);
            Finish();
        }
    }
}
