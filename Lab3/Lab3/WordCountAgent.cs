﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class WordCountAgent : Agent
    {
        private int id;
        public Dictionary<String, int> slownik;
        private List<String> words;
        private ReadFileAgent rFileAgent;
        private int N; // N of agents
        private int index, count; // where to start, and how many

        
        public WordCountAgent( int id, ReadFileAgent rFileAgent, int N ) : base(id)
        {
            this.id = id;
            slownik = new Dictionary<string, int>();
            this.rFileAgent = rFileAgent;
            this.N = N;
        }
        
        // piekna funkcja ktorej zadaniem jest robic krok
        public override void Update()
        {
            // jesli nie skonczyl wczytywac to pomin
            if (!rFileAgent.HasFinished) return;
            // ustalic zakres trzeba
            int nWords = rFileAgent.words.Capacity;
            this.count = nWords / this.N;
            // trzeba ustalić poczatkowy index (tez wykorzystamy id)
            this.index = (id - 1) * this.count;
            // jesli to jest ostatni agent to ma leciec do konca (wykorzystamy id)
            if (this.id == this.N) this.count += nWords % this.N;


            // jesli skonczyl wczytywac to trzeba pobrac slowa z przeznaczonego zakresu
            this.words = rFileAgent.words.GetRange(index, count);
            for( int i = 0; i < words.Count; i++ )
            {
                // najpierw trzeba pominac spacje i inne takie
                //if (words[i] == " " || words[i] == "\n" || words[i] == "\t" || words[i] == "") i++;
                // jesli slownik posiada klucz o i-tym slowie to zwieksz licznik o jeden
                if (slownik.ContainsKey(words[i]))
                    slownik[ words[i] ]++;
                // w innym przypadku trzeba dodac nowy wpis z wartoscia 1 (pierwsze wystapienie)
                else
                    slownik.Add(words[i], 1);
            }
            Finish();
        }
    }
}
