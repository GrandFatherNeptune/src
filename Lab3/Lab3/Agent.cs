﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    public abstract class Agent : IRunnable
    {
        //--- POLA ---//
        
        // krok czasu z jakim ma odswiezac
        protected float timeStep;

        // pomocniczy kolezka
        protected float virtualTimeS = 0.0f;

        // numer identyfikacyjny Agenta
        public int Id { get; private set; }

        //--- KONSTRUKTOR ---//
        public Agent( int id, float timeStep = 0.1f )
        {
            this.timeStep = timeStep;
            this.Id = id;
        }


        //--- METODY ---//

        // metoda Finish ktora konczy prace
        public void Finish() { HasFinished = true; }

        // implementacja interfejsu IRunnable i rozszerzenie o private set
        public bool HasFinished { get; private set; }

        // implementacja interfejsu IRunnable
        public IEnumerator<float> CoroutineUpdate()
        {
            while( !HasFinished )
            {
                Update();
                virtualTimeS += timeStep;
                if (HasFinished) yield break;
                else yield return virtualTimeS;
            }
        }
        
        // implementacja interejsu IRunnable
        public void Run()
        {
            while ( !HasFinished )
            {
                Update();
                virtualTimeS += timeStep;
                Thread.Sleep((int)Math.Round(timeStep * 1000.0f));
            }
        }

        public abstract void Update();
    }
}
