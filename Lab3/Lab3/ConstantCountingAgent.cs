﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    // zad (2.2)-4
    public class ConstantCountingAgent : Agent
    {
        // prywatny licznik
        private int counter = 0;

        public ConstantCountingAgent(int id) : base(id) { }

        public override void Update()
        {
            if ( ++counter >= 10 )
            {
                Console.WriteLine("I counted to 10 and my Id is {0}", this.Id);
                Finish();
            }            
        }
    }
}
