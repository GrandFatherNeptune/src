﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class SumAgent : Agent
    {
        public int sum;
        private int E;
        private int currE;
        private List<int> list;

        public SumAgent(int id, List<int> list, int E = 1) : base(id) {
            currE = 0;
            sum = 0;
            this.E = E;
            this.list = list;
        }

        public override void Update()
        {
            for( int i = currE*list.Count/E; i < currE*list.Count/E + list.Count/E; i++)
            {
                sum += list[i];
            }
            currE++;
            if (currE >= E) Finish();
        }
    }
}
