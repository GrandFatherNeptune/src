﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class Program
    {

        public static Mutex mutex = new Mutex();

        static void Main(string[] args)
        {
            /*// 2.2-6
            // tworzymy agentow
            List<IRunnable> runnables = GenerateRunnables(4,20000,2);
            // odpalamy ich
            RunThreads(runnables);
            //*/

            /*// 2.3
            // tworzymy agentow
            List<IRunnable> runnables = GenerateRunnables();
            // odpalamy ich
            RunFibers(runnables);
            //*/

            /*// Striiingasy
            // tworzymy agentow
            List<IRunnable> runnables = GenerateRunnables(4, 1, 1);
            // odpalamy towarzystwo i czekamy aż ostatni skonczy (master nasz)
            RunThreads(runnables).Last().Join();
            //*/

            //*// Let's rob a BANK
            List<IRunnable> runnables = GenerateBankRunnables();
            RunThreads(runnables).Last().Join();


            //*/


            // tu powinien byc Thread.Join() ale nie ma jak 
           // while (!runnables.Last().HasFinished) { } 

            // czekaj na BUTTON
            // Thread.Sleep(100);
            Console.WriteLine("End of program");
            Console.ReadKey();
        }

        static List<IRunnable> GenerateBankRunnables()
        {
            List<IRunnable> runnables = new List<IRunnable>();
            // BANK
            BankAgent bank = new BankAgent(0, 1000);
            runnables.Add(bank);
            // HAKERSI
            for ( int i = 1; i <= 300; i+=2 )
            {
                runnables.Add(new HakerAgentAdd(i, bank));
                runnables.Add(new HakerAgentDiv(i + 1, bank));
            }

            return runnables;
        }
        
        static List<IRunnable> GenerateRunnables()
        {
            /*/ punkt 2.2
            int N = 100;
            var runnables = new List<IRunnable>();
            int id = 0;
            // generujemy countingAgentow
            for ( ; id < N; id++ )
                runnables.Add(new SineGeneratingAgent(id));

            // generujemy SineGeneratingAgentow
            for (; id < 2*N; id++)
                runnables.Add(new CountingAgent(id));

            // generujemy ConstCountingAgentow
            for (; id < 3*N; id++)
                runnables.Add(new ConstantCountingAgent(id));

            //*/

            //*/ taki tam punkt 2.3
            var randoms = new List<int>();
            Random r = new Random();
            var runnables = new List<IRunnable>();
            // tutaj generacja 1000 losowych liczb
            for ( int i = 0; i < 1000; i++)
            {
                randoms.Add(r.Next(-10, 10));
                //randoms.Add(1);
            }
            for ( int i = 0; i < 4; i++ )
            {
                runnables.Add(new SumAgent(i, randoms.GetRange(i * randoms.Count / 4, randoms.Count / 4)));
            }
            runnables.Add(new SumMasterAgent(5, runnables.GetRange(0, 4)));
            //*/

            return runnables;
        }

        static List<IRunnable> GenerateRunnables(int N, int M, int E)
        {
            var runnables = new List<IRunnable>();

            /*/ taki tam punkt 2.3
            var randoms = new List<int>();
            Random r = new Random();
            // tutaj generacja M losowych liczb
            for (int i = 0; i < M; i++)
            {
                //randoms.Add(r.Next(-10, 10));
                randoms.Add(1);
            }
            for (int i = 0; i < N; i++)
            {
                runnables.Add(new SumAgent(i, randoms.GetRange(i * randoms.Count / N, randoms.Count / N)));
            }
            runnables.Add(new SumMasterAgent(N+1, runnables.GetRange(0, N)));
            //*/

            //*/ punkt ten ze stringami
            // dodajemy czytacza jako pierwszy agent
            var czytacz = new ReadFileAgent(0, "nie.txt");
            runnables.Add(czytacz);
            // dodajemy agentow zliczajacych dane etapy
            List<WordCountAgent> counters = new List<WordCountAgent>();
            for( int i = 1; i <= N; i++)
            {
                counters.Add(new WordCountAgent(i, czytacz, N));
            }
            // dodajemy finalnego agenta ktory sumuje wszytko
            var master = new WordCountMaster(N + 1, counters);

            // wszystko dodajemy jako lista runnables
            runnables.AddRange(counters);
            runnables.Add(master);
            //*/

            return runnables;
        }

        static List<Thread> RunThreads(IEnumerable<IRunnable> runnables)
        {
            var threads = new List<Thread>(runnables.Count());

            foreach ( var runnable in runnables )
            {
                var t = new Thread(runnable.Run);
                threads.Add(t);
                t.Start();
            }

            return threads;
        }

        static void RunFibers( IEnumerable<IRunnable> runnables )
        {
            var timeStep = 0.0f;

            var enumerators = runnables.Select(r => r.CoroutineUpdate());

            bool allFinished = false;
            while ( !allFinished )
            {
                foreach ( var enumerator in enumerators )
                {
                    // zrob jeden krok dla kazdego agenta
                    if (enumerator.MoveNext())
                    {
                        timeStep = enumerator.Current;
                    }

                    allFinished = !runnables.Any(r => !r.HasFinished);
                    //Thread.Sleep(10);
                }
            }
        }
    }
}
