﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    class HakerAgentDiv : Agent
    {
        public float balance;
        public BankAgent bank;


        public HakerAgentDiv(int id, BankAgent bank, float timeStep = 0.5F) : base(id, timeStep)
        {
            this.bank = bank;
        }

        public override void Update()
        {
            //*/ MUTEX CODE
            // czekaj az mutex sie zwolni a potem zajmij KOOD
            bank.mutex.WaitOne();
            // o ten wrazliwy
            try
            {
                balance = bank.balance / 2;
                bank.balance = balance;
                Console.WriteLine(">> DIV: " + balance);
            } 
            catch ( Exception excp )
            {
                Console.WriteLine( excp.StackTrace );
            }
            finally
            {
                // zwolnij mutex
                bank.mutex.ReleaseMutex();
            }
            //*/

            /*// LOCK CODE
            lock(bank)
            {
                balance = bank.balance / 2;
                bank.balance = balance;
                Console.WriteLine(">> DIV: " + balance);
            }
            //*/

            /*/ SPIN LOCK CODE
            bool spinLockTaken = false;
            try
            {
                bank.spinLock.Enter(ref spinLockTaken);
                balance = bank.balance / 2;
                bank.balance = balance;
                Console.WriteLine(">> DIV: " + balance);

            }
            catch (Exception excp)
            {
                Console.WriteLine(excp.StackTrace);
            }
            finally
            {
                if (spinLockTaken)
                {
                    bank.spinLock.Exit();
                }
            }
            //*/

            /*/ SPIN LOCK CODE -> busy-waiting
            bool spinLockTaken = false;
            while (!spinLockTaken)
            {
                bank.spinLock.Enter(ref spinLockTaken);
            }
            if (spinLockTaken)
            {
                // do some stuff
                try
                {
                    balance = bank.balance / 2;
                    bank.balance = balance;
                    Console.WriteLine(">> DIV: " + balance);
                }
                finally
                {
                    // zwolnij kolege
                    bank.spinLock.Exit();
                }
            }
            //*/

            if (bank.HasFinished) Finish();
        }
    }
}
