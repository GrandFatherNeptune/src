﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrzybyszSzymanski
{
    // zad (2.2)-4
    public class CountingAgent : Agent
    {
        // prywatny licznik
        private int counter = 0;

        public CountingAgent(int id) : base(id) { }

        public override void Update()
        {
            if (++counter >= this.Id)
            {
                Console.WriteLine("I counted to {0} and now I quit", counter);
                Finish();
            }
        }
    }
}
